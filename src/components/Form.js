import { useEffect, useState } from "react";

const Form = () => {
    const [firstname, setFirstName] = useState(localStorage.getItem("firstname" || ""));
    const [lastname, setLastName] = useState(localStorage.getItem("lastname" || ""));

    const firstnameInputChangeHandler = (event) => {
        setFirstName(event.targert.value);
    }
    const lastnameInputChangeHandler = (event) => {
        setLastName(event.targert.value);
    }

    useEffect(() => {
        localStorage.setItem("firstname", firstname);
        localStorage.setItem("lastname", lastname);
    }, [firstname, lastname])
    return(
        <div>
            <input value={firstname} onChange={firstnameInputChangeHandler} />
            <br />
            <input value={lastname} onChange={lastnameInputChangeHandler} />
            <p> {firstname} {lastname}</p>
        </div>
    )
}

export default Form;